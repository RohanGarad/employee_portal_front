import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {


  user = new User();

  constructor(private _service : RegistrationService ,private _router :Router) { }

  ngOnInit(): void {
  }

  updateUser()
  {
    this._service.updateUserFromRemote(this.user).subscribe(
      data=>{
        console.log("response Recived");
        alert("succesfully updated");
        this._router.navigate(['/login']);
      }
    )
  }

}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {User} from './user';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private _http : HttpClient){ }
  public loginUserFromRemote(user :User):Observable<any>{
    return this._http.post<any>("http://18.213.180.10:8887/login",user)


  }

public registerUserFromRemote(user :User):Observable<any>{
  return this._http.post<any>("http://18.213.180.10:8887/registeruser",user)
}

public updateUserFromRemote(user : User):Observable<any>{
  return this._http.put<any>("http://18.213.180.10:8887/update",user)
}

}

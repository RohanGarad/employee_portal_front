import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Feed} from '../feed';
import { FeedService } from '../feed.service';
 
@Component({
  selector: 'app-loginsuccess',
  templateUrl: './loginsuccess.component.html',
  styleUrls: ['./loginsuccess.component.css']
})
export class LoginsuccessComponent implements OnInit {

  feed = new  Feed();
  feeds!: Feed[];
  constructor(private route :ActivatedRoute, private service :FeedService ,private router :Router ) { }
   you =this.route.snapshot.params['id'];
  ngOnInit(): void {

    this.service.getfeed().subscribe((data:Feed[])=>
    {
      console.log(data);
      this.feeds=data;
    })
  }
  postFeed()
  {
    this.service.postFeedFromRemote(this.feed).subscribe(
      data=>
      {
        alert("sended post");
      this.router.navigate(['/loginsuccess']);
    },
    error=> {
      
      console.log("Exception occured");
    }
    )
  }
  gotoupdate()
  {
    this.router.navigate(['/update']);
  }
}

import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

   user = new User();
   
   msg= ""
  constructor( private _service :RegistrationService ,private _router: Router) { }
  
  ngOnInit(): void {
  }
  loginUser()
  {
   this._service.loginUserFromRemote(this.user).subscribe(
     data=>{
       
      alert("Login successfullll");
      this._router.navigate(['/loginsuccess']);

    },
     error=>{alert("Exception Occured");
     this.msg="Bad Credential # please Enter Valid Password and Valid EmailID";
  
  
    }
   )
  }

  gotoregistration()
  {
    alert("Hello Buddy..");
    this._router.navigate(['/registration']);
  }

}

import { Injectable, ɵclearResolutionOfComponentResourcesQueue } from '@angular/core';
import { from, Observable } from 'rxjs';

import {HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';
import { Feed } from './feed';
@Injectable({
  providedIn: 'root'
})
export class FeedService {

  url ="http://18.213.180.10:8887/feed";

  constructor(private http :HttpClient ) {   }
 getfeed():Observable<Feed[]>{
   return this.http.get<Feed[]>(`${this.url}`);
 }

  public postFeedFromRemote(feed : Feed):Observable<any>
  {
    return this.http.post<any>("http://18.213.180.10:8887/savepost",feed);
  }
}
